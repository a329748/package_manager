const log4js = require("log4js");

let logger = log4js.getLogger();

logger.level = "debug";

logger.info("The application has launched successfully.");
logger.warn("The application is missing libraries.");
logger.error("The function specified was not found.");
logger.fatal("The application has failed to launch.");

function sum(x, y){
    return x + y;
}

module.exports = sum;