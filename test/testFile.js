const sum = require("../index");
const assert = require("assert");

describe("Testing the sum of two numbers: ", ()=>{
    it("5 + 7 is 12", ()=>{
        assert.equal(12, sum(5, 7));
    });
    
    it("5 + 7 is not 57", ()=>{
        assert.notEqual("57", sum(5, 7));
    });
});